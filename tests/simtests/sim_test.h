//
// Created by crojjang on 4/23/20.
//

#ifndef GLOW_SIM_TEST_H
#define GLOW_SIM_TEST_H


#include <fcntl.h>
#include <unistd.h>
#include <iostream>



int conv1_test();
int conv2_test();
int conv3_test();
int conv4_test();
int conv5_test();
int conv6_test();
int conv7_test();
int conv8_test();
int conv9_test();
int conv10_test();
int conv11_test();
int conv12_test();
int conv13_test();
int conv14_test();
int conv15_test();
int conv16_test();
int conv17_test();
int conv18_test();
int conv19_test();
int conv20_test();
int conv21_test();
int conv_2layer_test();
int add_16_test();
int gemm_16_test();
int gemm_gemm_16_test();
#endif //GLOW_SIM_TEST_H
