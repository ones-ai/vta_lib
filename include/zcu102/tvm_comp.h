#ifndef TVM_RUNTIME_COMP_C_RUNTIME_API_H_
#define TVM_RUNTIME_COMP_C_RUNTIME_API_H_

#ifndef TVM_DLL
#define TVM_DLL  __attribute__((visibility("default")))
#endif


#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
}  // TVM_EXTERN_C
#endif

#endif
